package Graded1;

import java.util.Arrays;

public class User {
	String userName;
	int userId;
	String emailId;
	String password;
	String newBooks;
    String favourite;
    String completed;
	
	public User(String userName, int userId, String emailId, String password, String newBooks, String favourite,
			String completed) {
		super();
		this.userName = userName;
		this.userId = userId;
		this.emailId = emailId;
		this.password = password;
		this.newBooks = newBooks;
		this.favourite = favourite;
		this.completed = completed;
	}
	public String getNewBooks() {
		return newBooks;
	}
	public void setNewBooks(String newBooks) {
		this.newBooks = newBooks;
	}
	public String getFavourite() {
		return favourite;
	}
	public void setFavourite(String favourite) {
		this.favourite = favourite;
	}
	public String getCompleted() {
		return completed;
	}
	public void setCompleted(String completed) {
		this.completed = completed;
	}
	
	public String getUserName() {
		return userName;
	}
	@Override
	public String toString() {
		return "User [userName=" + userName + ", userId=" + userId + ", emailId=" + emailId + ", password=" + password
				+ ", newBooks=" + newBooks + ", favourite=" + favourite + ", completed=" + completed + "]";
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	


}
